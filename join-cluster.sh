#!/bin/bash
# --stdin=false, --tty=false
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "node_count":"3"}'
kubectl exec -it couchdb-1 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "node_count":"3"}'
kubectl exec -it couchdb-2 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "node_count":"3"}'

# kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "port": 5984, "node_count": "3", "remote_node": "couchdb-0.couchdb-service>", "remote_current_user": "admin", "remote_current_password": "password" }'
# kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "add_node", "host":"couchdb-0.couchdb-service", "port": 5984, "username": "admin", "password":"password"}'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "port": 5984, "node_count": "3", "remote_node": "couchdb-1.couchdb-service>", "remote_current_user": "admin", "remote_current_password": "password" }'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "add_node", "host":"couchdb-1.couchdb-service", "port": 5984, "username": "admin", "password":"password"}'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "port": 5984, "node_count": "3", "remote_node": "couchdb-2.couchdb-service>", "remote_current_user": "admin", "remote_current_password": "password" }'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "add_node", "host":"couchdb-2.couchdb-service", "port": 5984, "username": "admin", "password":"password"}'
kubectl exec -it couchdb-2 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "finish_cluster"}'
kubectl exec -it couchdb-0 -- curl http://admin:password@127.0.0.1:5984/_membership
kubectl exec -it couchdb-1 -- curl http://admin:password@127.0.0.1:5984/_membership
kubectl exec -it couchdb-2 -- curl http://admin:password@127.0.0.1:5984/_membership

#kubectl exec -it couchdb-2 -- curl  http://admin:password@127.0.0.1:5984/_cluster_setup