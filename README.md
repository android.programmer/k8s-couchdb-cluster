# k8s-couchdb-cluster

This projects will spin up a Couchdb cluster of 3 nodes on K8s. This will guide you through step by step.

## Introduction
Apache Couchdb is a document-based NoSQL database which is written in Erlang. Erlang, which is created by Ericsson,(Ericsson Language) is a functional programming language and is designed to be fault tolerant and scalabe
thru its Erlang processes, immutable data structure and message passing model. It can achieve the same with much less code when comparing to OOP language like Java and C while having scalability in place due to Erlang's nature. It gained more popularity when Whatsapp discussed it's core messaging system is built on the top of [Ejabberd](https://en.wikipedia.org/wiki/Ejabberd).


## Prerequisite:
For development purpose, we use minikube.
- Kubernetes 
- [Minikube](https://minikube.sigs.k8s.io/docs/)

### 1. Define storage config
```
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: couchdb-vol-0
  labels:
    volume: couchdb-volume
spec:
  capacity:
    storage: 8Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: YOUR_PATH
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: couchdb-vol-1
  labels:
    volume: couchdb-volume
spec:
  capacity:
    storage: 8Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: YOUR_PATH
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: couchdb-vol-2
  labels:
    volume: couchdb-volume
spec:
  capacity:
    storage: 8Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: YOUR_PATH
```
### 2. Define statefulsets config
While creating a CouchDB cluster does not require ordering of the nodes, it needs stable, persistent storage. So we choose to use statefulsets as workload resource.

### 3. Define services config
```
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: couchdb
spec:
  selector:
      matchLabels:
        app: sit-back-and-relax  
  serviceName: "couchdb-service"
  replicas: 3
  template:
    metadata:
      labels:
        app: sit-back-and-relax # pod label
    spec:
      containers:
      - name: couchdb
        image: couchdb:3.1.1
        env:
        - name: NODE_NETBIOS_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: NODENAME
          value: $(NODE_NETBIOS_NAME).couchdb-service #FQDN in /opt/couchdb/data/vm.args
        - name: COUCHDB_USER
          value: admin
        - name: COUCHDB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: couchdbpassword
              key: COUCHDB_PASSWORD
        - name: COUCHDB_SECRET
          value: cookie
        - name: ERL_FLAGS
          value: "-name couchdb@$(NODENAME)"
        - name: ERL_FLAGS
          value: "-setcookie cookie" #magic cookie is used for authentication among nodes in an erlang cluster.
        ports:
        - name: couchdb
          containerPort: 5984
        - name: epmd
          containerPort: 4369
        - containerPort: 9100
        volumeMounts:
          - name: couchdb-pvc
            mountPath: /opt/couchdb/data 
  volumeClaimTemplates:
  - metadata:
      name: couchdb-pvc
    spec:
      accessModes: ["ReadWriteOnce"]
      resources:
        requests:
          storage: 8Gi
      selector:
        matchLabels:
          volume: couchdb-volume
```
### 4. CouchDB join cluster

Couchdb does not have the concept of a "master" node in a cluster.
On each node we need to run the following. If the number of nodes is large enough, it is recommended to use configuration management tools like Ansible/Chef/Puppet.
```
curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "node_count":"3"}'

```
and then to join a node in a cluster, run a http request for each node you want to add in a particular node
(it is called "setup coordination node" in CouchDB's term.)
```
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "port": 5984, "node_count": "3", "remote_node": "couchdb-1.couchdb-service>", "remote_current_user": "admin", "remote_current_password": "password" }'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "add_node", "host":"couchdb-1.couchdb-service", "port": 5984, "username": "admin", "password":"password"}'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "enable_cluster", "bind_address":"0.0.0.0", "username": "admin", "password":"password", "port": 5984, "node_count": "3", "remote_node": "couchdb-2.couchdb-service>", "remote_current_user": "admin", "remote_current_password": "password" }'
kubectl exec -it couchdb-0 -- curl -X POST -H "Content-Type: application/json" http://admin:password@127.0.0.1:5984/_cluster_setup -d '{"action": "add_node", "host":"couchdb-2.couchdb-service", "port": 5984, "username": "admin", "password":"password"}'
```

Verify our installation:
```
curl http://admin:password@127.0.0.1:5984/_membership
```
Response:
```
{
    "all_nodes": [
        "couchdb-1.couchdb-service",
        "couchdb-2.couchdb-service",
        "couchdb-0.couchdb-service"
    ],
    "cluster_nodes": [
        "couchdb-1.couchdb-service",
        "couchdb-2.couchdb-service",
        "couchdb-0.couchdb-service"
    ]
}
```

If "all_nodes" equals "cluster_nodes", we will be able to connect to our cluster.

We could use `join-cluster.sh`  to do the above tasks

https://docs.couchdb.org/en/stable/setup/cluster.html



## Takeaways
- Kubernetes is a system to deploy containerized apps
- **Nodes** are individual machines (or vm's) that run containers
- **Master** ("k8s.io/kubernetes/cmd/kube-apiserver/app") are machines (or vm's) with a set of programs to mange nodes
- Kubernetes does not build our images - it got them from other repoistories
- To deploy something, we update the desired state of the master with a config file
- The master works constantly to meet our desired state